package dv.spring.demo.repository

import dv.spring.demo.entity.ShoppingCart
import org.springframework.data.repository.CrudRepository

interface ShoppingCartRepository:CrudRepository<ShoppingCart,Long>