package dv.spring.demo.util

import dv.spring.demo.entity.*
import dv.spring.demo.entity.dto.*
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers

@Mapper (componentModel = "spring")
interface MapperUtil{
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }
@Mappings(
        Mapping(source = "manufacturer",target = "manu")
)
    fun mapProductDto(product: Product?):ProductDto?

    fun mapProductDto(product: List<Product>):List<ProductDto>

    fun mapManufacturer(manu: Manufacturer): ManufacturerDto

//    fun mapCustomerDto(customer: Customer):Customer?

    fun mapCustomerDto(customer: Customer?):CustomerDto

    fun mapCustomerDto(customer: List<Customer>): List<CustomerDto>

    fun mapShoppingCartDto(shoppingCart: ShoppingCart):ShoppingCartDto

    fun mapShoppingCartDto(shoppingCart: List<ShoppingCart>):List <ShoppingCartDto>

    fun mapSelectedProductDto(selectedProduct: SelectedProduct):SelectedProductDto
    fun mapSelectedProductDto(selectedProduct: List<SelectedProduct>):List<SelectedProductDto>

    fun mapAddress(address: Address): AddressDto

    @InheritInverseConfiguration
    fun mapManufacturer(manu: ManufacturerDto): Manufacturer


    @InheritInverseConfiguration
    fun mapAddress(address:AddressDto):Address

    @InheritInverseConfiguration
    fun mapProductDto(productDto: ProductDto): Product

    @InheritInverseConfiguration
    fun mapCustomerDto(customerDto: CustomerDto): Customer
}