package dv.spring.demo.dao

import dv.spring.demo.entity.Manufacturer
import dv.spring.demo.repository.ManufacturerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class ManufacturerDaoImpl:ManufaturerDao{
    override fun finById(id: Long): Manufacturer? {
      return manufacturerRepository.findById(id).orElse(null)
    }

    override fun getManufacturers(): List<Manufacturer> {
        return mutableListOf(Manufacturer("Apple","053123456"),
                Manufacturer("Samsung","555666777888"),
                Manufacturer("CAMT","0000000"))
    }
    @Autowired
    lateinit var manufacturerRepository: ManufacturerRepository
    override fun save(manufacturer: Manufacturer): Manufacturer {
        return manufacturerRepository.save(manufacturer)
    }
}