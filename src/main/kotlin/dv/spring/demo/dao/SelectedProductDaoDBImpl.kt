package dv.spring.demo.dao


import dv.spring.demo.entity.SelectedProduct
import dv.spring.demo.repository.SelectedProductRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class SelectedProductDaoDBImpl: SelectedProductDao{
    override fun getSelectedProductByProductNameWithPage(name: String, page: Int, pageSize: Int): Page<SelectedProduct> {
        return selectedProductRepository.findByProduct_NameContainingIgnoreCase(name,PageRequest.of(page,pageSize))
    }

    override fun getSelectedProductByProductName(name: String): List<SelectedProduct> {
        return selectedProductRepository.findByProduct_NameContainingIgnoreCase(name)
    }

    override fun getSelectedProducts(): List<SelectedProduct> {
        return selectedProductRepository.findAll().filterIsInstance(SelectedProduct::class.java)
    }


    @Autowired
    lateinit var selectedProductRepository: SelectedProductRepository




}