package dv.spring.demo.dao

import dv.spring.demo.entity.Product
import dv.spring.demo.entity.ShoppingCart
import dv.spring.demo.repository.ProductRepository
import dv.spring.demo.repository.ShoppingCartRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class ShoppingCartDaoDBImpl: ShoppingCartDao{
    @Autowired
    lateinit var shoppingCartRepository: ShoppingCartRepository

    override fun getShoppingCarts(): List<ShoppingCart> {
        return shoppingCartRepository.findAll().filterIsInstance(ShoppingCart::class.java)
    }
}

