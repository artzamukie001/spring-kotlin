package dv.spring.demo.dao

import dv.spring.demo.entity.Address
import dv.spring.demo.repository.AddressRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class AddressDaoDBImpl:AddressDao{
    override fun findById(id: Long): Address? {
        return addressRepository.findById(id).orElse(null)
    }

    @Autowired
   lateinit var addressRepository: AddressRepository
  override fun save(address:Address): Address{
      return addressRepository.save(address)
  }
}