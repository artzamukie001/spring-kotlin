package dv.spring.demo.dao

import dv.spring.demo.entity.Address


interface AddressDao{
    fun save(address: Address): Address
    fun findById(id: Long): Address?

}