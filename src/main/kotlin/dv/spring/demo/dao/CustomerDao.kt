package dv.spring.demo.dao

import dv.spring.demo.entity.Customer

interface CustomerDao{
    fun getCustomers():List<Customer>
    fun getCustomerByName(name:String): Customer?
    fun getCustomerByPartialName(name: String): List<Customer>
    fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer>
    fun getCustomerByManuName(name: String): List<Customer>
    fun save(customer: Customer): Customer
    fun findById(id: Long): Customer?
}
