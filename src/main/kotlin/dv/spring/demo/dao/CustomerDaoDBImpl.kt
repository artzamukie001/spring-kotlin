package dv.spring.demo.dao

import dv.spring.demo.entity.Customer
import dv.spring.demo.repository.CustomerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class CustomerDaoDBImpl: CustomerDao{
    override fun getCustomerByManuName(name: String): List<Customer> {
        return customerRepository.findByDefaultAddress_ProvinceContainingIgnoreCase(name)
    }

    override fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer> {
        return customerRepository.findByNameContainingIgnoreCaseOrEmailContainingIgnoreCase(name, email)
    }

    override fun getCustomerByPartialName(name: String): List<Customer> {
        return customerRepository.findByNameContainingIgnoreCase(name)
    }

    @Autowired
    lateinit var customerRepository:CustomerRepository
    override fun getCustomers(): List<Customer> {
//        return customerRepository.findAll().filterIsInstance(Customer::class.java)
        return customerRepository.findByIsDeletedIsFalse()
    }

    override fun getCustomerByName(name: String): Customer? {
        return customerRepository.findByName(name)
    }

    override fun save(customer: Customer): Customer {
        return customerRepository.save(customer)
    }

    override fun findById(id:Long): Customer? {
        return customerRepository.findById(id).orElse(null)
    }
}
