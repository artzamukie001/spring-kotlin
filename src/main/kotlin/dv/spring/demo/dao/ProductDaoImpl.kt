//package dv.spring.demo.dao
//
//import dv.spring.demo.entity.Manufacturer
//import dv.spring.demo.entity.Product
//import org.springframework.context.annotation.Profile
//import org.springframework.stereotype.Repository
//
//@Profile("mem")
//@Repository
//class ProductDaoImpl:ProductDao{
//    override fun getProducts(): List<Product> {
//        return mutableListOf(Product("iPhone",
//                "It's a phone",
//                28000.00,
//                20,
//                Manufacturer("Apple","000000000"),
//                "https://www.jaymartstore.com/Products/iPhone-X-64GB-Space-Grey--1140900010552--4724"),
//                Product("Note9",
//                        "Other Iphone",
//                        28001.00,
//                        10,
//                        Manufacturer("Samsung","987654321"),
//                        "http://dynamic-cdn.eggdigital.com/e56zBiUt1.jpg"),
//                Product("CAMT",
//                        "The best College in CMU",
//                        0.00,
//                        1,
//                        Manufacturer("CAMT","123456789"),
//                        "http://www.camt.cmu.ac.th/th/images/logo.jpg"),
//                Product("Prayuth",
//                        "The best PM ever",
//                        1.00,
//                        1,
//                        Manufacturer("CAMT","123456789"),
//                        "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Prayut_Chan-o-cha_%28cropped%29_2016.jpg/200px-Prayut_Chan-o-cha_%28cropped%29_2016.jpg"))
//    }
//}