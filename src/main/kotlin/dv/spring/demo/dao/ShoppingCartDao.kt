package dv.spring.demo.dao

import dv.spring.demo.entity.ShoppingCart

interface ShoppingCartDao{
    fun getShoppingCarts():List<ShoppingCart>
}
