package dv.spring.demo.dao

import dv.spring.demo.entity.Manufacturer

interface ManufaturerDao {
    fun getManufacturers(): List<Manufacturer>

    fun save(manufacturer: Manufacturer): Manufacturer
    fun finById(id: Long): Manufacturer?
}