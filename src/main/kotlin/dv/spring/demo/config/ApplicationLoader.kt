package dv.spring.demo.config

import dv.spring.demo.entity.*
import dv.spring.demo.repository.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import javax.transaction.Transactional

@Component
class ApplicationLoader: ApplicationRunner{
    @Autowired
    lateinit var manufacturerRepository: ManufacturerRepository
    @Autowired
    lateinit var productRepository: ProductRepository
    @Autowired
    lateinit var addressRepository: AddressRepository
    @Autowired
    lateinit var customerRepository: CustomerRepository
    @Autowired
    lateinit var selectedProductRepository: SelectedProductRepository
    @Autowired
    lateinit var shoppingCartRepository: ShoppingCartRepository
    @Autowired
    lateinit var dataLoader: DataLoader

    @Transactional
    override fun run(args: ApplicationArguments?) {
        var manul = manufacturerRepository.save(Manufacturer("CAMT","0000000"))
        var manul2 = manufacturerRepository.save(Manufacturer("SAMSUNG","1111111"))
        var manul3 = manufacturerRepository.save(Manufacturer("Apple","2222222"))

        var address1 = addressRepository.save(Address("ถนนอนุสาวรีย์ประชาธิปไตย","แขวง ดินสอน", "เขตดุสิต","กรุงเทพ","10123"))
        var address2 = addressRepository.save(Address("239 มหาวิทยาลัยเชียงใหม่"," ต.สุเทพ","อ.เเมือง","เชียงใหม่","50200"))
        var address3 = addressRepository.save(Address("ซักที่บนโลก","ต.สุขสันต์","อ.ในเมือง","ขอนแก่น","12457"))

        var cus1 = customerRepository.save(Customer("Lung","pm@go.th",UserStatus.ACTIVE))
        var cus2 = customerRepository.save(Customer("ชัชชาติ","chut@taopoon.com",UserStatus.ACTIVE))
        var cus3 = customerRepository.save(Customer("ธนาธร","thanathorn@life.com",UserStatus.PENDING))

        var selected1 = selectedProductRepository.save(SelectedProduct(1))
        var selected2 = selectedProductRepository.save(SelectedProduct(2))
        var selected4 = selectedProductRepository.save(SelectedProduct(4))
        var selected5 = selectedProductRepository.save(SelectedProduct(1))
        var selected6 = selectedProductRepository.save(SelectedProduct(1))

        var shop1 = shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.SENT))
        var shop2 = shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.WAIT))


        val product1 = productRepository.save(Product("CAMT",
                "The best College in CMU",
                0.0,
                1,
                "https://www.camt.cmu.ac.th/th/images/logo.jpg"))

        var product2 = productRepository.save(Product("Note9",
                "Other Iphone",
                28001.00,
                10,
                "http://dynamic-cdn.eggdigital.com/e56zBiUt1.jpg"))

        var product3 = productRepository.save(Product("Ipone",
                "It's a phone",
                28000.00,
                20,
                "https://www.jaymartstore.com/Products/iPhone-X-64GB-Space-Grey--1140900010552--4724"))

        var product4 = productRepository.save(Product("Prayuth",
                "The best PM ever",
                1.00,
                1,
                "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Prayut_Chan-o-cha_%28cropped%29_2016.jpg/200px-Prayut_Chan-o-cha_%28cropped%29_2016.jpg"))

        manul.products.add(product1)
        product1.manufacturer = manul

        manul2.products.add(product2)
        product2.manufacturer = manul2

        manul3.products.add(product3)
        product3.manufacturer = manul

        manul.products.add(product4)
        product4.manufacturer = manul

        cus1.defaultAddress = address1
        cus2.defaultAddress = address2
        cus3.defaultAddress = address3

        selected4.product = product3
        selected1.product = product4
        selected5.product = product1
        selected2.product = product2
        selected6.product = product1

        shop1.customer = cus1
        shop1.selectedProduct.add(selected4)
        shop1.selectedProduct.add(selected1)

        shop2.customer = cus2
        shop2.selectedProduct.add(selected5)
        shop2.selectedProduct.add(selected6)
        shop2.selectedProduct.add(selected2)

        dataLoader.loadData()
    }
}