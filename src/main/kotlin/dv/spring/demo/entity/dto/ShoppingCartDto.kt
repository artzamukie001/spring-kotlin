package dv.spring.demo.entity.dto

import dv.spring.demo.entity.Customer
import dv.spring.demo.entity.SelectedProduct
import dv.spring.demo.entity.ShoppingCartStatus

data class ShoppingCartDto(
        var shoppingCartStatus: ShoppingCartStatus? = ShoppingCartStatus.WAIT,
        var selectedProduct: List<SelectedProductDto>? = mutableListOf<SelectedProductDto>(),
        var customer : CustomerDto? = null)