package dv.spring.demo.entity

import javax.persistence.*

@Entity
data class ShoppingCart(var shoppingCartStatus: ShoppingCartStatus? = ShoppingCartStatus.WAIT){
    @Id
    @GeneratedValue
    var id:Long? = null
    @OneToMany
    var selectedProduct = mutableListOf<SelectedProduct>()
    @OneToOne
    var shippingAddress : Address? = null
    @OneToOne
    var customer : Customer? = null
}