package dv.spring.demo.controller

import dv.spring.demo.entity.dto.AddressDto
import dv.spring.demo.service.AddressService
import dv.spring.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.jpa.domain.AbstractPersistable_.id
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class AddressController {
    @Autowired
    lateinit var addressService: AddressService
    @PostMapping("/address")
    fun addAddress(@RequestBody address: AddressDto)
            : ResponseEntity<Any>{
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapAddress(addressService.save(address)))
    }

    @PutMapping("/address/{addressId}")
    fun updateAddress(@PathVariable("addressId") id:Long?,
                    @RequestBody address: AddressDto)
        : ResponseEntity<Any>{
        address.id = id
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapAddress(
                        addressService.save(address)
                )
        )
    }
}
