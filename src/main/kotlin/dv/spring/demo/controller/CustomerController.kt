package dv.spring.demo.controller

import dv.spring.demo.entity.dto.CustomerDto
import dv.spring.demo.service.CustomerService
import dv.spring.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.xml.ws.Response

@RestController
class CustomerController{

    @Autowired
    lateinit var customerService: CustomerService
    @GetMapping("/customer")
    fun getAllCustomer():ResponseEntity<Any>{
        val customers = customerService.getCustomers()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapCustomerDto(customers))
    }

    @GetMapping("/customer/query")
    fun getCustomers(@RequestParam("name")name:String):ResponseEntity<Any>{
        var output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByName(name)!!)
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/customers/partialQuery")
    fun getCustomersPartial(@RequestParam("name")name:String,
                            @RequestParam(value = "email", required = false) email:String?):ResponseEntity<Any>{
        val output = MapperUtil.INSTANCE.mapCustomerDto(
                customerService.getCustomerByPartialNameAndEmail(name,name))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customerProvince")
    fun getCustomerByManuName(@RequestParam("province")name:String):ResponseEntity<Any>{
        val output = MapperUtil.INSTANCE.mapCustomerDto(
                customerService.getCustomerByManuName(name))
        return ResponseEntity.ok(output)
    }

    @PostMapping("/customer")
    fun addCustomer(@RequestBody customerDto: CustomerDto)
        :ResponseEntity<Any>{
        val output = customerService.save(MapperUtil.INSTANCE.mapCustomerDto(customerDto))
        val outputDto = MapperUtil.INSTANCE.mapCustomerDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @PostMapping("/customer/address/{addressId}")
    fun addCustomer(@RequestBody customerDto: CustomerDto,
                    @PathVariable addressId:Long)
        :ResponseEntity<Any>{
        val output = customerService.save(addressId,
                MapperUtil.INSTANCE.mapCustomerDto(customerDto))
        val outputDto = MapperUtil.INSTANCE.mapCustomerDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @DeleteMapping("/customer/{id}")
    fun deleteCustomer(@PathVariable("id")id:Long):ResponseEntity<Any>{
        val customer = customerService.remove(id)
        val outputCustomer = MapperUtil.INSTANCE.mapCustomerDto(customer)
        outputCustomer?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the customer id is not found")
    }

}