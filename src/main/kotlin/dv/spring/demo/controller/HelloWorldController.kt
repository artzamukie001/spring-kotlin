//package dv.spring.demo.controller
//
//import dv.spring.demo.entity.Person
//import dv.spring.demo.entity.muPersons
//import org.springframework.http.ResponseEntity
//import org.springframework.web.bind.annotation.*
//
//@RestController
//class HelloWorldController {
//    @GetMapping("/helloWorld")
//    fun getHelloWorld(): String {
//        return "HelloWorld"
//    }
//
//    @GetMapping("/test")
//    fun getTest(): String {
//        return "CAMT"
//    }
//
//    @GetMapping("/person")
//    fun getPerson(): ResponseEntity<Any> {
//        val person = Person("Somchai", "Somrak", 15)
//        return ResponseEntity.ok(person)
//    }
//
//    @GetMapping("/persons")
//    fun getPersons(): ResponseEntity<Any> {
//        val persons1 = Person("Somchai", "Somrak", 15)
//        val persons2 = Person("Prayut", "Chan", 60)
//        val persons3 = Person("Lung", "Pom", 65)
//        val persons = listOf<Person>(persons1, persons2, persons3)
//        return ResponseEntity.ok(persons)
//    }
//
//    @GetMapping("/MyPerson")
//    fun getMyPerson(): ResponseEntity<Any> {
//        val Myperson = Person("Sarawut", "Boonruang", 22)
//        return ResponseEntity.ok(Myperson)
//    }
//
//    @GetMapping("/myPersons")
//    fun getmyPersons(): ResponseEntity<Any> {
//        val persons1 = muPersons("Osora", "Tsubasa", "Nachansu", 10)
//        val persons2 = muPersons("Hyuoka", "Kojiro", "Meiwa", 9)
//        val persons = listOf<muPersons>(persons1, persons2)
//        return ResponseEntity.ok(persons)
//    }
//
//    @GetMapping("/params")
//    fun getParams(@RequestParam("name") name: String, @RequestParam("surname") surname: String)
//            : ResponseEntity<Any> {
//         return ResponseEntity.ok("$name $surname")
//    }
//
//    @GetMapping("/params/{name}/{surname}/{age}")
//    fun getPathParam(@PathVariable("name") name:String,
//                     @PathVariable("surname") surname:String,
//                     @PathVariable("age") age:Int):ResponseEntity<Any>{
//        val person = Person(name, surname, age)
//        return ResponseEntity.ok(person)
//    }
//
//    @PostMapping("/echo")
//    fun echo(@RequestBody person: Person) : ResponseEntity<Any>{
//        return ResponseEntity.ok(person)
//    }
//}
//
//
//
//
//
//
