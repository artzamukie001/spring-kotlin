package dv.spring.demo.service

import dv.spring.demo.entity.Product
import org.springframework.data.domain.Page
import javax.transaction.Transactional

interface ProductService{
    fun getProducts():List<Product>
    fun getProductByName(name:String): Product?
    fun getProductByPartialName(name: String): List<Product>
    fun getProductByPartialNameAndDesc(name: String, desc: String): List<Product>
    fun getProductByManuName(name: String): List<Product>
    fun getProductWithPage(name: String, page: Int, pageSize: Int): Page<Product>
    fun save(product: Product): Product
    fun save(manuId: Long, product: Product): Product
    @Transactional
    fun remove(id: Long): Product?
}

