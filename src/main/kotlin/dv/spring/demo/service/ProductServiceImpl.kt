package dv.spring.demo.service

import dv.spring.demo.dao.ManufaturerDao
import dv.spring.demo.dao.ProductDao
import dv.spring.demo.entity.Product
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Transactional
@Service
class ProductServiceImpl : ProductService {
    override fun getProductWithPage(name: String, page: Int, pageSize: Int): Page<Product> {
        return productDao.getProductWithPage(name, page, pageSize)
    }

    override fun getProductByManuName(name: String): List<Product> {
        return productDao.getProductByManuName(name)
    }

    override fun getProductByPartialName(name: String): List<Product> {
        return productDao.getProductByPartialName(name)
    }

    @Autowired
    lateinit var productDao: ProductDao

    override fun getProductByName(name: String): Product? = productDao.getProductByName(name)

    override fun getProducts(): List<Product> {
        return productDao.getProducts()
    }

    override fun getProductByPartialNameAndDesc(name: String, desc: String): List<Product> {
        return productDao.getProductByPartialNameAndDesc(name, desc)
    }

//    override fun save(product: Product): Product {
//        return productDao.save(product)
//    }

    @Autowired
    lateinit var manufacturerDao: ManufaturerDao
    override fun save(product: Product): Product{
        val manufacturer = product.manufacturer?.let { manufacturerDao.save(it) }
        val product = productDao.save(product)
        manufacturer?.products?.add(product)
        return product
    }

    @Transactional
    override fun save(manuId: Long, product: Product): Product{
        val manufacturer = manufacturerDao.finById(manuId)
        val product = productDao.save(product)
        product.manufacturer = manufacturer
        manufacturer?.products?.add(product)
        return product
    }

    @Transactional
    override fun remove(id:Long): Product? {
        val product = productDao.findById(id)
        product?.isDeleted = true
        return product
    }
}
