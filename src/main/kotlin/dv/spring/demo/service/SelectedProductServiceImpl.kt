package dv.spring.demo.service

import dv.spring.demo.dao.SelectedProductDao
import dv.spring.demo.entity.SelectedProduct
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class SelectedProductServiceImpl:SelectedProductService{
    override fun getSelectedProductByProductNameWithPage(name: String, page: Int, pageSize: Int): Page<SelectedProduct> {
        return selectedProductDao.getSelectedProductByProductNameWithPage(name, page, pageSize)
    }

    override fun getSelectedProductByProductName(name: String): List<SelectedProduct> {
        return selectedProductDao.getSelectedProductByProductName(name)
    }

    override fun getSelectedProducts(): List<SelectedProduct> {
        return  selectedProductDao.getSelectedProducts()
    }

    @Autowired
    lateinit var selectedProductDao: SelectedProductDao
}
