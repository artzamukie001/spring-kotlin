package dv.spring.demo.service

import dv.spring.demo.dao.AddressDao
import dv.spring.demo.entity.Address
import dv.spring.demo.entity.dto.AddressDto
import dv.spring.demo.util.MapperUtil

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AddressServiceImpl:AddressService{
    @Autowired
    lateinit var addressDao: AddressDao

    override fun  save(address: AddressDto): Address {
        val address = MapperUtil.INSTANCE.mapAddress(address)
        return addressDao.save(address)
    }
}