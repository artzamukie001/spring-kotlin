package dv.spring.demo.service

import dv.spring.demo.dao.ManufaturerDao
import dv.spring.demo.entity.Manufacturer
import dv.spring.demo.entity.dto.ManufacturerDto
import dv.spring.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ManufacturerServiceImpl:ManufacturerService{
    override fun save(manu: ManufacturerDto): Manufacturer {
        val manufacturer = MapperUtil.INSTANCE.mapManufacturer(manu)
        return manufacturerDao.save(manufacturer)
    }

    @Autowired
    lateinit var manufacturerDao: ManufaturerDao
    override fun getManufacturers(): List<Manufacturer> {
        return manufacturerDao.getManufacturers()
    }
}