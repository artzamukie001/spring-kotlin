package dv.spring.demo.service

import dv.spring.demo.entity.Address
import dv.spring.demo.entity.dto.AddressDto


interface AddressService {
    fun save(address: AddressDto): Address
//    fun save(address: AddressDto): Address
}
