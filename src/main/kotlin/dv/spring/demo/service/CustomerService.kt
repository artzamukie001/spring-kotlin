package dv.spring.demo.service

import dv.spring.demo.entity.Customer
import javax.transaction.Transactional

interface CustomerService{
    fun getCustomers():List<Customer>
    fun getCustomerByName(name:String): Customer?
    fun getCustomerByPartialName(name: String): List<Customer>
    fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer>
    fun getCustomerByManuName(name: String): List<Customer>
    fun save(mapCustomerDto: Customer): Customer

    @Transactional
    fun save(addressId: Long, customer: Customer): Customer

    @Transactional
    fun remove(id: Long): Customer?
}
