package dv.spring.demo.service

import dv.spring.demo.entity.SelectedProduct
import org.springframework.data.domain.Page

interface SelectedProductService{
    fun getSelectedProducts():List<SelectedProduct>
    fun getSelectedProductByProductName(name: String): List<SelectedProduct>
    fun getSelectedProductByProductNameWithPage(name: String, page: Int, pageSize: Int): Page<SelectedProduct>

}
