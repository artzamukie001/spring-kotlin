package dv.spring.demo.service

import dv.spring.demo.entity.Manufacturer
import dv.spring.demo.entity.dto.ManufacturerDto
import dv.spring.demo.util.MapperUtil

interface ManufacturerService{
    fun getManufacturers(): List<Manufacturer>
    fun save(manu: ManufacturerDto): Manufacturer
}