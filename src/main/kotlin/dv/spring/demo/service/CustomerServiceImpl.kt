package dv.spring.demo.service

import dv.spring.demo.dao.AddressDao
import dv.spring.demo.dao.CustomerDao
import dv.spring.demo.entity.Customer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Transactional
@Service
class CustomerServiceImpl:CustomerService{
    override fun getCustomerByManuName(name: String): List<Customer> {
        return customerDao.getCustomerByManuName(name)
    }

    override fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer> {
        return customerDao.getCustomerByPartialNameAndEmail(name,email)
    }

    override fun getCustomerByPartialName(name: String): List<Customer> {
        return customerDao.getCustomerByPartialName(name)
    }

    @Autowired
    lateinit var customerDao:CustomerDao

    override fun getCustomerByName(name: String): Customer?
        = customerDao.getCustomerByName(name)


    override fun getCustomers(): List<Customer> {
        return customerDao.getCustomers()
    }

//    override fun save(customer: Customer):Customer{
//        return customerDao.save(customer)
//    }

    @Autowired
    lateinit var addressDao: AddressDao
    override fun save(customer: Customer): Customer{
        val address = customer.defaultAddress?.let { addressDao.save(it) }
        val customer = customerDao.save(customer)
        customer?.defaultAddress = address
        return customer
    }

    @Transactional
    override fun save(addressId:Long,customer: Customer): Customer {
        val address = addressDao.findById(addressId)
        val customer = customerDao.save(customer)
        customer?.defaultAddress = address
        return customer
    }

    @Transactional
    override fun remove(id:Long): Customer? {
        val customer = customerDao.findById(id)
        customer?.isDeleted = true
        return customer
    }
}
